<?php namespace PLC\Portal;

use Backend;
use System\Classes\PluginBase;

/**
 * Portal Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Portal',
            'description' => 'No description provided yet...',
            'author'      => 'PLC',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'PLC\Portal\Components\NoticeCenter' => 'noticeCenter',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'plc.portal.some_permission' => [
                'tab' => 'Portal',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'portal' => [
                'label'       => 'Portal',
                'url'         => Backend::url('plc/portal/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['plc.portal.*'],
                'order'       => 500,
            ],
        ];
    }

}
