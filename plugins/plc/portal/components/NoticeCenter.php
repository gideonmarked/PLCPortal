<?php namespace PLC\Portal\Components;

use Cms\Classes\ComponentBase;
use October\Rain\Parse\Bracket;
use Log;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;
use Session;

class NoticeCenter extends ComponentBase
{
    public $notice;
    public $calendar;
    public $todo;
    public $attendance;
    public $tasks;
    public $reports;
    public $sports;
    public $profile;
    public $settings;
    public $calendars;
    public $calendarContent;
    public $calendar_disabled_settings;
    public $calendar_settings_items;

    public function componentDetails()
    {
        return [
            'name'        => 'NoticeCenter Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [

            'notice' => [
                'title' => 'Notice',
                'description' => 'Enable notice component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'calendar' => [
                'title' => 'Calendar',
                'description' => 'Enable calendar component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'todo' => [
                'title' => 'To Do',
                'description' => 'Enable todo component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'attendance' => [
                'title' => 'Attendance',
                'description' => 'Enable attendance component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'tasks' => [
                'title' => 'Tasks',
                'description' => 'Enable tasks component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'reports' => [
                'title' => 'Reports',
                'description' => 'Enable reports component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'sports' => [
                'title' => 'Sports',
                'description' => 'Enable sports component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'profile' => [
                'title' => 'Profile',
                'description' => 'Enable profile component.',
                'type' => 'checkbox',
                'default' => true
            ],
            'settings' => [
                'title' => 'Settings',
                'description' => 'Enable settings component.',
                'type' => 'checkbox',
                'default' => true
            ],

        ];
    }

    public function onRun()
    {
        $this->addJs('components/noticecenter/assets/plugin.js');
        $this->addJs('components/noticecenter/assets/autoload.js');
        $this->addCss('components/noticecenter/assets/css/style.css');


        $this->notice = $this->property('notice');
        $this->calendar = $this->property('calendar');
        $this->todo = $this->property('todo');
        $this->attendance = $this->property('attendance');
        $this->tasks = $this->property('tasks');
        $this->reports = $this->property('reports');
        $this->sports = $this->property('sports');
        $this->profile = $this->property('profile');
        $this->settings = $this->property('settings');

        Session::forget('calendar_disabled_settings');
    }

    public function getNoticeFeed()
    {
        $notices = [];
        $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/edu_json.php?sql=02notices';
        $client = new Client();
        $request = $client->request('POST', $api_url, [
                'verify' => false,
                'form_params' => [
                    'FROMROW' => 0,
                    'TOROW' => 15,
                ]
            ]);
        $notices = json_decode($request->getBody());
        $notice_items = [];
        foreach($notices as $notice)
        {
            
            $item = (array)$notice;
            //$item['CONTENT'] = Bracket::parse($item['CONTENT']);
            $notice_items[] = $item;
        }
        return $notice_items;
    }

    public function getCalendarFeed()
    {

        $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/edu_json.php?sql=03calendar';
        // $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/endpointtest.php';
        $calendar_items = [];

        $client = new Client();
        $request = $client->request('POST', $api_url, [
                'verify' => false,
                'form_params' => [
                    'FROMROW' => 1,
                    'TOROW' => 12,
                ]
            ]);


        
        

        $calendar_items = [];
        foreach(json_decode($request->getBody()) as $calendar)
        {
            $disabled = false;
            if(Session::get('calendar_disabled_settings'))
            {
                $this->calendar_disabled_settings = Session::get('calendar_disabled_settings');
                foreach ($this->calendar_disabled_settings as $setting) {
                    if( $calendar->AUDIENCE_TYPE_ID == $setting)
                        $disabled = true;
                }
            }
            if( !$disabled )
            {
                $item = (array)$calendar;
                $start_date = explode('-', $item['Start date']);
                $item['start_date'] = $start_date[2] . ' ' . $this->convertToMonthName( $start_date[1] );
                $item['start_year'] = $start_date[0];
                $start_time = explode(':', $item['start time']);
                $item['start_time'] = ($start_time[0] > 12 ? $start_time[0] - 12 : $start_time[0] ) . ':' . $start_time[1];
                $item['start_period'] = ($start_time[0] >= 12 ? 'PM' : 'AM' );
                $end_time = explode(':', $item['End time']);
                $item['end_time'] = ($end_time[0] > 12 ? $end_time[0] - 12 : $end_time[0] ) . ':' . $end_time[1];
                $item['end_period'] = ($end_time[0] >= 12 ? 'PM' : 'AM' );
                $calendar_items[] = $item;
            }
        }
        return $calendar_items;
    }

    public function onLoadCalendarContent()
    {
        $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/edu_json.php?sql=03calendar';
        // $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/endpointtest.php';
        $calendar_items = [];

        $client = new Client();
        $request = $client->request('POST', $api_url, [
                'verify' => false,
                'form_params' => [
                    'FROMROW' => post('row'),
                    'TOROW' => post('row'),
                ]
            ]);


        
        

        $calendar_items = [];
        $list = json_decode($request->getBody());
        foreach($list[0] as $key => $val)
        {
            switch ($key) {
                case 'Start date':
                    $start_date = explode('-', $val);
                    $calendar_items['start_date'] = $start_date[2] . ' ' . $this->convertToMonthName( $start_date[1] );
                    $calendar_items['start_year'] = $start_date[0];
                    break;

                case 'start time':
                    $start_time = explode(':', $val);
                    $calendar_items['start_time'] = ($start_time[0] > 12 ? $start_time[0] - 12 : $start_time[0] ) . ':' . $start_time[1];
                    $calendar_items['start_period'] = ($start_time[0] >= 12 ? 'PM' : 'AM' );
                    break;

                case 'End time':
                    $end_time = explode(':', $val);
                    $calendar_items['end_time'] = ($end_time[0] > 12 ? $end_time[0] - 12 : $end_time[0] ) . ':' . $end_time[1];
                    $calendar_items['end_period'] = ($end_time[0] >= 12 ? 'PM' : 'AM' );
                    break;
                
                default:
                    $calendar_items[$key] = $val;
                    break;
            }
        }

        $this->calendarContent = $calendar_items;
    }

    public function onCalendarSettings()
    {
        $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/audience_colour.php';
        // $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/endpointtest.php';
        $calendar_items = [];

        $client = new Client();
        $request = $client->request('GET', $api_url, [
                'verify' => false,
            ]);


        
        $this->calendar_disabled_settings = Session::get('calendar_disabled_settings');

        $this->calendar_settings_items = [];
        foreach(json_decode($request->getBody()) as $item)
        {
            $item = (array)$item;
            if( isset($this->calendar_disabled_settings) && in_array($item["AUDIENCE_TYPE_ID"], $this->calendar_disabled_settings) )
                $item['disabled'] = true;
            else
                $item['disabled'] = false;
            $this->calendar_settings_items[] = $item;

        }
    }

    public function onCalendarSettingsUpdate()
    {
        $this->calendar_disabled_settings = (Session::get('calendar_disabled_settings') ? Session::get('calendar_disabled_settings') : []);
        $key = array_search( post('id') , $this->calendar_disabled_settings);
        if( post('disabled') == 'false' && $key > -1 )
                unset($this->calendar_disabled_settings[0]);
        else if( post('disabled') == 'true' && !$key )
            $this->calendar_disabled_settings[] = post('id');

        // var_dump($this->calendar_disabled_settings);

        Session::put('calendar_disabled_settings', $this->calendar_disabled_settings);

        $this->onCalendarSettings();
    }

    private function convertToMonthName( $month )
    {
        $month_list = [
            '01' => 'JAN',
            '02' => 'FEB',
            '03' => 'MAR',
            '04' => 'APR',
            '05' => 'MAY',
            '06' => 'JUN',
            '07' => 'JUL',
            '08' => 'AUG',
            '09' => 'SEP',
            '10' => 'OCT',
            '11' => 'NOV',
            '12' => 'DEC',
        ];

        return $month_list[ $month ];
    }


    public function onConvertString( $content )
    {
        return html_entity_decode($content);
    }

    public function onConvertStringExcerpt( $content )
    {
        return strip_tags(html_entity_decode($content));
    }

    public function onBack()
    {
        
    }
}