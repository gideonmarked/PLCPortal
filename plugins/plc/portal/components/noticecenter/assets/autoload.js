$(document).ready(function() {
	var win = $(window);
	var loading = false;
	var count = 0;

	$('#loading').hide();
	$('#calendar-main').hide();

	// Each time the user scrolls
	win.scroll(function() {
		// End of the document reached?
		if ($(document).height() - win.height() == win.scrollTop() && !loading) {
			loading = true;
			$('#loading').show();
			console.log('should load');

			$.ajax({
				url: 'load-calendar/' + count,
				dataType: 'html',
				success: function(html) {
					$('#calendar-items').append(html);
					$('#loading').hide();
					loading = false;
					count++;
				}
			});
		}
	});
});