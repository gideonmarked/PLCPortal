$(document).ready(function(){
    $('.centers').hide();
    // $('#calendarcenter').show();
    // $('.components').hide();
    $('#noticecenter-menu').click(function(){
        $('#noticecenter').slideDown();
        $('.components').slideUp();
    });
    $('#calendarcenter-menu').click(function(){
        $('#calendarcenter').slideDown();
        $('.components').slideUp();
    });
    $('.home-link').click(function(){
        $('.centers').slideUp();
        $('.components').slideDown();
    });
    $('.notice-content').click(function(){
        var title = $(this).find('h6').html();
        var content = $(this).find('span.text').html();
        $('#notice-main').find('.title').html( title );
        $('#notice-main').find('.text').html(content);
        $('#notice-main').slideDown();
        $('.center-item').slideUp();
    });
    $('.center-footer a.back-link').click(function(){
        $(this).closest('.centers').find('.center-item').slideDown();
        $(this).closest('.centers').find('#notice-main').slideUp();
    });
    $('.centers .notice-text a').click(function(){
        return false;
    })
});