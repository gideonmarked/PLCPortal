<?php
use GuzzleHttp\Client;
Route::get('load-calendar/{count}', function ($count) {

    $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/edu_json.php?sql=03calendar';
    // $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/endpointtest.php';
    $calendar_items = [];

    $client = new Client();
    $request = $client->request('POST', $api_url, [
            'verify' => false,
            'form_params' => [
                'FROMROW' => 3 + ($count * 10),
                'TOROW' => 12 + ($count * 10),
            ]
        ]);

    $items = '';

    foreach(json_decode($request->getBody()) as $calendar)
    {
        $item = (array)$calendar;
        $items .= "<a href=\"#\" data-request=\"onLoadCalendarContent\" data-request-data=\"row: " . $item['ROWNUM'] . "\" data-request-update=\"'noticeCenter::calendar-content': '#calendarcenter'\">";
        $items .= "<div class=\"col-md-12 center-item\">";
        $items .= "<div class=\"col-md-3\">";
        $start_date = explode('-', $item['Start date']);
        $items .= "<div class=\"col-md-12 start-date\">" . $start_date[2] . ' ' . convertToMonthName( $start_date[1] ) . "</div>";
        $items .= "<div class=\"col-md-12 start-year\">" . $start_date[0] . "</div>";
        $items .= "</div>";
        $items .= "<div class=\"col-md-1\">";
        $start_time = explode(':', $item['start time']);
        $items .= "<div class=\"col-md-12 start-time\">" . ($start_time[0] > 12 ? $start_time[0] - 12 : $start_time[0] ) . ':' . $start_time[1] . "</div>";
        $items .= "<div class=\"col-md-12 start-period\">" . ($start_time[0] >= 12 ? 'PM' : 'AM' ) . "</div>";
        $items .= "</div>";
        $items .= "<div class=\"col-md-8\">";
        $items .= "<div class=\"col-md-12 description\">" . $item['Description'] . "</div>";
        $items .= "<div class=\"col-md-12 location\">" . $item['Location'] . "</div>";
        $items .= "</div>";
        $items .= "</div>";
        $items .= "</a>";
    }
    return $items;
});

Route::get('load-notification/{count}', function ($count) {

    $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/edu_json.php?sql=03calendar';
    // $api_url = 'https://edumate.plc.nsw.edu.au/plc/PLC_CUSTOM_DEV/PORTALDEV/endpointtest.php';
    $calendar_items = [];

    $client = new Client();
    $request = $client->request('POST', $api_url, [
            'verify' => false,
            'form_params' => [
                'FROMROW' => 3 + ($count * 10),
                'TOROW' => 12 + ($count * 10),
            ]
        ]);

    $items = '';
    foreach(json_decode($request->getBody()) as $calendar)
    {
        // $item = (array)$calendar;
        // $items .= "<a href="#" data-request="onLoadCalendarContent" data-request-data="row: {{ schedule.ROWNUM }}" data-request-update="\'calendar-content\': \'#calendarcenter\'">";
        // $items .= "<div class="col-md-12 center-item">";
        // $items .= "<div class="col-md-3">";
        // $start_date = explode('-', $item['Start date']);
        // $items .= "<div class="col-md-12 start-date">" . $start_date[2] . ' ' . convertToMonthName( $start_date[1] ) . "</div>";
        // $items .= "<div class="col-md-12 start-year">" . $start_date[0] . "</div>";
        // $items .= "</div>";
        // $items .= "<div class="col-md-1">";
        // $start_time = explode(':', $item['start time']);
        // $items .= "<div class="col-md-12 start-time">" . ($start_time[0] > 12 ? $start_time[0] - 12 : $start_time[0] ) . ':' . $start_time[1] . "</div>";
        // $items .= "<div class="col-md-12 start-period">" . ($start_time[0] >= 12 ? 'PM' : 'AM' ) . "</div>";
        // $items .= "</div>";
        // $items .= "<div class="col-md-8">";
        // $items .= "<div class="col-md-12 description">" . $item['Description'] . "</div>";
        // $items .= "<div class="col-md-12 location">" . $item['Location'] . "</div>";
        // $items .= "</div>";
        // $items .= "</div>";
        // $items .= "</a>";
    }
    return $items;
});

function convertToMonthName( $month )
    {
        $month_list = [
            '01' => 'JAN',
            '02' => 'FEB',
            '03' => 'MAR',
            '04' => 'APR',
            '05' => 'MAY',
            '06' => 'JUN',
            '07' => 'JUL',
            '08' => 'AUG',
            '09' => 'SEP',
            '10' => 'OCT',
            '11' => 'NOV',
            '12' => 'DEC',
        ];

        return $month_list[ $month ];
    }

